﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoETargetBullet : Bullet
{
    public float SplashRadius;
	public bool CanSlow;
	public float SlowDuration;
	public float SlowPercent;
	public bool CanDoT;
	public float BurnDuration;
	public int BurnDamage;
    public override void Explode()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, SplashRadius);
        for (int i = 0; i < hitColliders.Length; i++)
        {
			if (hitColliders[i].tag == "Enemy" && hitColliders[i].GetComponent<EnemyType>().Type == Type.Ground)
            {
                int damage = (int)Random.Range(minDamage, maxDamage);

                hitColliders[i].GetComponent<EnemyStats>().ReceiveDamage(damage);
				if (CanDoT) {
					hitColliders [i].GetComponent<EnemyStats> ().StartBurning (BurnDuration, BurnDamage);

				}
				if (CanSlow) {
					hitColliders [i].GetComponent<EnemyStats> ().StartSlowing (SlowDuration, SlowPercent);
				}
            }
        }

    }
}
