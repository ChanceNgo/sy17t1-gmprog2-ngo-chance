﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTargetBullet : Bullet {

    public override void Explode()
    {
        int damage = (int)Random.Range(minDamage, maxDamage);
        Target.ReceiveDamage(damage);
    }
}
