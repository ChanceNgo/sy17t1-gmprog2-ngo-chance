﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apocalypse : Skill {
	public override void Update()
	{
		base.Update ();
	}

	public override void Cast(){
		base.Cast ();
		FindObjectOfType<Spawner> ().KillAllSpawns ();
	}
}
