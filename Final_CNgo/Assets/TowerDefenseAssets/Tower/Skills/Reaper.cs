﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaper : Skill {

	public override void Update()
	{
		base.Update ();

		if (Cooldown >= CooldownCopy) {
			Cast ();
		}
	
		//Cast ();
	

	}


	public override void Cast(){

		if (FindObjectOfType<Spawner> ().LowestHealthEnemy ().tag == "Enemy") {

			GameObject temp = FindObjectOfType<Spawner> ().LowestHealthEnemy ();
			float damage = temp.GetComponent<EnemyStats> ().HP * 25 / 100;

			Collider[] hitColliders = Physics.OverlapSphere(temp.transform.position, 2);
			temp.GetComponent<EnemyStats> ().ReceiveDamage (temp.GetComponent<EnemyStats> ().MaxHP);
			for (int i = 0; i < hitColliders.Length; i++)
			{
				if (hitColliders[i].tag == "Enemy")
				{

					hitColliders[i].GetComponent<EnemyStats>().ReceivePercentDamage(damage);

				}
			}
			Cooldown = 0;
		}
		Debug.Log("Skill Casted");

	}

}
