﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill : MonoBehaviour {
	public float Cooldown;
	protected float CooldownCopy;
	// Use this for initialization
	void Start () {
		CooldownCopy = Cooldown;
	}
	
	// Update is called once per frame
	public virtual void Update () {
		if(Cooldown < CooldownCopy)		
		{

			Cooldown += Time.deltaTime;
		}
	}

	public virtual void Cast()
	{
		if (Cooldown < CooldownCopy) {
			Debug.Log ("CLEAR");
			return;
		}
		Cooldown = 0;

	}
}
