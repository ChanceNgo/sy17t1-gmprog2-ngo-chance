﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Meteor : Skill {

	public bool IsSelectingTarget;
	Vector3 targetPos;
	public override void Update()
	{
		base.Update ();
		if (IsSelectingTarget) {
			if (Cooldown < CooldownCopy)
				return;
			
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
			{
				Debug.Log("Skill Target");

				// if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask, QueryTriggerInteraction.Ignore))//, LayerMask.NameToLayer("Towers")))//, QueryTriggerInteraction.Collide))
				if(Physics.Raycast(ray, out hit, Mathf.Infinity))
				{
					Cast ();
					Collider[] hitColliders = Physics.OverlapSphere(hit.transform.position, 2);
					for (int i = 0; i < hitColliders.Length; i++)
					{
						if (hitColliders[i].tag == "Enemy" && hitColliders[i].GetComponent<EnemyType>().Type == Type.Ground)
						{

							hitColliders[i].GetComponent<EnemyStats>().ReceivePercentDamage(85);

						}
					}
					IsSelectingTarget = false;
				}
			}
		}

	}

	public void SelectTarget()
	{
		IsSelectingTarget = true;

	}

	public override void Cast(){
		base.Cast ();
		Debug.Log("Skill Casted");
	
	}
}
