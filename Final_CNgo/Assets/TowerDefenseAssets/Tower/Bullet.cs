﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public EnemyStats Target;
    public float moveSpeed;
    public int minDamage;
    public int maxDamage;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        Move();       
    }

    public void Move()
    {
        if (Target != null)
        {
            if (Vector3.Distance(transform.position, Target.transform.position) <= 0.1f)
            {
                Explode();
                Destroy(gameObject);
            }
            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, moveSpeed * Time.deltaTime); 
        }
        else
        {
            Debug.Log("No target");
            Destroy(gameObject);
        }
    }

    public virtual void Explode()
    {
       
    }

}
