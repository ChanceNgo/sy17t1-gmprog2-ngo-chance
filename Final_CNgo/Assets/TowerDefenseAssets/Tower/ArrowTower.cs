﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTower : Tower {

    public override void Update()
    {
        base.Update();
        ChooseTarget();
    }

    public override void ChooseTarget() {
        base.ChooseTarget();

    }

    public override void Fire()
    {
    
    }

     void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyStats>() != null)
        {
            Enemies.Add(other.GetComponent<EnemyStats>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Enemies.Remove(other.GetComponent<EnemyStats>());
    }

}
