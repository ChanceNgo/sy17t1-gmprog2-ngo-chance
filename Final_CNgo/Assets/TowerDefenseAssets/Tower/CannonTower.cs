﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonTower : Tower {
    public float SplashRadius;
    public override void Update()
    {
        base.Update();
        ChooseTarget();
    }

    public override void ChooseTarget() {
        base.ChooseTarget();
        Projectile.GetComponent<AoETargetBullet>().SplashRadius = SplashRadius;

    }

    public override void Fire()
    {
    
    }

     void OnTriggerEnter(Collider other)
    {
		if (other.GetComponent<EnemyStats>() != null && other.GetComponent<EnemyType>().Type == Type.Ground)
        {
            Enemies.Add(other.GetComponent<EnemyStats>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
		if (other.GetComponent<EnemyStats>() != null && other.GetComponent<EnemyType>().Type == Type.Ground)
			Enemies.Remove(other.GetComponent<EnemyStats>());
    }

}
