﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTower : Tower {
	public float SplashRadius;
	public float BurnDuration;
	public int BurnDamage;

	public override void Update()
	{
		base.Update();
		ChooseTarget();
	}

	public override void ChooseTarget() {
		base.ChooseTarget();
		Projectile.GetComponent<AoETargetBullet>().SplashRadius = SplashRadius;
		Projectile.GetComponent<AoETargetBullet> ().CanDoT = true;
		Projectile.GetComponent<AoETargetBullet> ().BurnDuration = BurnDuration;
		Projectile.GetComponent<AoETargetBullet> ().BurnDamage = BurnDamage;
	}

	public override void Fire()
	{

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<EnemyStats>() != null)
		{
			Enemies.Add(other.GetComponent<EnemyStats>());
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<EnemyStats>() != null )
			Enemies.Remove(other.GetComponent<EnemyStats>());
	}


}
