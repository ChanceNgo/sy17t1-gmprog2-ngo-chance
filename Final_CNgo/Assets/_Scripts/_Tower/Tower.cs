﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tower : MonoBehaviour
{
    //Tower name
    public string Name;
    //Tower level
    public int Level;
    //Tower price
    public int Price;

    public int minDamage;
    public int maxDamage;

    public float FireRate;
    public float FireRateCopy;
    //Can Upgrade Tower
    public bool CanUpgrade;
    //Build Duration
    public float BuildDuration;
    //Material of the tower
    public Material towerMat;

    public GameObject Projectile;

    //Is the tower placed?
    public bool IsPlaced;
    //Is the tower selected?
    public bool IsSelected;

    //The possible upgrade of the tower
    public GameObject NextUpgrade;

    public List<EnemyStats> Enemies;
    // Use this for initialization
    private void Start()
    {
        FireRateCopy = FireRate;
    }
    public virtual void Update()
    {
        CanUpgrade = NextUpgrade != null ? true : false;

        //If the tower is selected...
        if (IsSelected)
        {
            //Set the material of the tower to cyan
           // towerMat.color = Color.cyan;
        }
        //Else set it back to normal
        else
        {
            //Set the material of the tower to white
          //  towerMat.color = Color.white;
        }

    }
    public virtual void ChooseTarget()
    {
        if (FireRate > FireRateCopy)
        {
            if (Enemies.Count > 0)
            {
                EnemyStats temp = new EnemyStats();
                for (int i = 0; i < Enemies.Count; i++)
                {
                    if (Enemies[i] != null)
                    {
                        temp = Enemies[i];
                        break;
                    }
                    else
                    {
                        Enemies.Remove(Enemies[i]);
                    }
                }
                for (int i = 1; i < Enemies.Count; i++)
                {
                    if (Enemies[i] == null)
                    {
                        Enemies.Remove(Enemies[i]);
                    }
                    if (Vector3.Distance(temp.transform.position, this.gameObject.transform.position) < Vector3.Distance(Enemies[i].transform.position, this.gameObject.transform.position))
                    {
                        temp = Enemies[i];
                    }
                }
                GameObject bullet = (GameObject)Instantiate(Projectile, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Quaternion.identity);
                bullet.GetComponent<Bullet>().Target = temp;
                bullet.GetComponent<Bullet>().minDamage = minDamage;
                bullet.GetComponent<Bullet>().maxDamage = maxDamage;
                FireRate = 0;
                //Enemies.Clear();
            }
        }
        else
        {
            FireRate += Time.deltaTime;
        }
    }
    public virtual void Fire()
    {
        
        
    }

    public virtual void Upgrade()
    {

        //There is a possible upgrade,
        if (NextUpgrade != null)
        {
            //Instantiate the new tower in the same place and rotation
            Instantiate(NextUpgrade, transform.position, Quaternion.identity);
            //And destroy the current tower
            Destroy(gameObject);
        }
    }

    public void Buildable()
    {
        //Set the material of the tower to green
        Debug.Log("Tower Buildable");
       // towerMat.color = Color.green;
    }

    public void NonBuildable()
    {
        //Set the material of the tower to red
        Debug.Log("Tower Not Buildable");
       // towerMat.color = Color.red;
    }

    public void Build()
    {
        //Set the tower as "placed"
        IsPlaced = true;
        //Set the material of the tower to white
       // towerMat.color = Color.white;
        //Enable the collider of the tower
        GetComponent<Collider>().enabled = true;
    }
}
