﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform SpawnPoint;
    public List<Spawn> SpawnList;
    public int CurrentWave;

    public int HpIncrease;
    public int GoldIncrease;

    private List<GameObject> spawnedList;
    private bool waveStarted;

    // Use this for initialization
    void Start()
    {
        spawnedList = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        //Faster
        //spawnedList.RemoveAll(x => x == null);

        for(int i = 0; i < spawnedList.Count; i++)
        {
            if(spawnedList[i] == null)
            {
                spawnedList.Remove(spawnedList[i]);
                break;
            }
        }

        if(!waveStarted)
        {
            Spawn(CurrentWave);
            waveStarted = true;
        }
        else if(waveStarted && spawnedList.Count <= 0)
        {
            waveStarted = false;
            CurrentWave++;
        }
        
    }

    private void Spawn(int index)
    {
        for (int i = 0; i < SpawnList[index].SpawnCount; i++)
        {
            GameObject spawned = (GameObject)Instantiate(SpawnList[index].SpawnPrefab, SpawnPoint.position, Quaternion.identity);
            spawned.GetComponent<EnemyStats>().HP = SpawnList[index].SpawnHP + (CurrentWave * HpIncrease);
            spawned.GetComponent<EnemyStats>().GoldReward = SpawnList[index].GoldReward + (CurrentWave * GoldIncrease);
            spawnedList.Add(spawned);
        }

    }

	public void KillAllSpawns()
	{
		foreach (GameObject spawn in spawnedList) {
			spawn.GetComponent<EnemyStats> ().SelfDestruct ();
		
		}
	}

	public GameObject LowestHealthEnemy()
	{
		List<GameObject> LowHealths = new List<GameObject> ();
		foreach (GameObject spawn in spawnedList) {
			if (spawn.GetComponent<EnemyStats> ().HP <= spawn.GetComponent<EnemyStats> ().MaxHP * 15 / 100) {
				LowHealths.Add (spawn);
			}

		}
		GameObject temp = null;

		if(LowHealths.Count > 0)
		temp = LowHealths [Random.Range (0, LowHealths.Count)];

		return temp;
	}
}
