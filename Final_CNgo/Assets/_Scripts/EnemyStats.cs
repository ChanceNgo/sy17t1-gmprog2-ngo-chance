﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public float HP;
    public float MaxHP;
    public int GoldReward;

	public bool IsSlowed;
	float SlowDuration;
	float SlowPercent;
	float SlowedSpeed;

	public bool IsBurning;
	float BurnDuration;
	int BurnDamage;

	// Use this for initialization
	void Start () {
        MaxHP = HP;
	}
	
	// Update is called once per frame
	void Update () {
        IsDead();
		Burning();
		Slowed ();
	}

    void IsDead()
    {
        if (HP <= 0)
        {
            GameObject.Find("GameManager").GetComponent<PlayerStats>().AddGold(GoldReward);
            Destroy(gameObject);

        }
    }

	public void StartBurning(float duration, int damage)
	{
		IsBurning = true;
		BurnDuration = duration;
		BurnDamage = damage;
	}

	public void StartSlowing(float duration, float slowPercent)
	{
		IsSlowed = true;
		SlowDuration = duration;
		SlowPercent = slowPercent;
		SlowedSpeed = GetComponent<Pathing> ().NavMesh.speed * SlowPercent / 100;
		GetComponent<Pathing> ().NavMesh.speed = SlowedSpeed;
	}
	void Burning()
	{
		if (IsBurning) {
			if (BurnDuration > 0) {
				BurnDuration -= Time.deltaTime;
				float damage = BurnDamage * Time.deltaTime;
				ReceiveDamage (damage);
			} else
				IsBurning = false;
		}
	}

	void Slowed()
	{
		if (IsSlowed) {
			if (SlowDuration > 0) {
				SlowDuration -= Time.deltaTime;

			} else {
				IsSlowed = false;
				GetComponent<Pathing> ().NavMesh.speed = GetComponent<Pathing> ().origSpeed;
			}
		}
	}

    public void ReceiveDamage(float val)
    {
        HP -= val;
        if (HP < 0)
            HP = 0;
    }

    public void ReceivePercentDamage(float percent)
    {
        float dmgVal = HP * percent/100;
        Debug.Log(dmgVal);
        HP -= dmgVal;
        if (HP < 0)
            HP = 0;

    }

	public void SelfDestruct()
	{
		ReceiveDamage (MaxHP);
	}
}
