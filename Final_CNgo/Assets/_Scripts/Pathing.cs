﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pathing : MonoBehaviour
{

    public Transform EndPoint;
    public NavMeshAgent NavMesh;
	public float origSpeed;
    // Use this for initialization
    void Start()
    {
        if (!NavMesh)
        {
            NavMesh = GetComponent<NavMeshAgent>();
			origSpeed = NavMesh.speed;
        }
        EndPoint = GameObject.Find("EndPoint").transform;
        NavMesh.destination = EndPoint.position;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = (EndPoint.position - transform.position).magnitude;
        if (dist <= 1) 
        {
            Destroy(gameObject);
        }
    }
}
