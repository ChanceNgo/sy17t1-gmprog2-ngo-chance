﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildManager : MonoBehaviour
{

    public LayerMask RaycastMask;

    Ray ray;
    RaycastHit hit;
    /*[SerializeField]
    GameObject prefabTower;*/
    [SerializeField]
    GameObject objectTower;
    [SerializeField]
    List<GameObject> Towers;

    public GameObject player;

    void Start()
    {

    }

    void Update()
    {
        SelectArea();
        if (objectTower != null)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Destroy(objectTower);
        }
    }

    public void CreateTower(string towerName)
    {
        GameObject Tower = Towers.Find(x => x.name.ToLower().Contains(towerName.ToLower()));
        GameObject twr = Instantiate(Tower) as GameObject;
        twr.name = Tower.name;
        if (objectTower != null)
            Destroy(objectTower);
        objectTower = twr;
    }

    public void CreateTower(int index)
    {
        GameObject twr = Instantiate(Towers[index]) as GameObject;
        twr.name = Towers[index].name;
        if (objectTower != null)
            Destroy(objectTower);
        objectTower = twr;
    }

    public void CreateTower(GameObject tower)
    {
        GameObject twr = Instantiate(tower) as GameObject;
        if (objectTower != null)
            Destroy(objectTower);
        objectTower = twr;
    }

    Vector3 SnapToGrid(Vector3 towerObject)
    {
        return new Vector3(Mathf.Round(towerObject.x),
                            towerObject.y,
                            Mathf.Round(towerObject.z));
    }

    void SelectArea()
    {
        if (objectTower != null)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100f, RaycastMask, QueryTriggerInteraction.Ignore))
            {
                Vector3 towerPos = hit.point;
                objectTower.transform.position = SnapToGrid(towerPos);
                //Debug.Log(hit.point);

                if (hit.point.y >= 2.2f && hit.point.y < 2.5f && player.GetComponent<PlayerStats>().Gold >= objectTower.GetComponent<Tower>().Price)
                {
                    objectTower.GetComponent<Tower>().Buildable();

                    if (Input.GetMouseButtonDown(0))
                    {
                        player.GetComponent<PlayerStats>().DeductGold(objectTower.GetComponent<Tower>().Price);
                        objectTower.GetComponent<Tower>().Build();
                        objectTower = null;
                    }
                }
                else
                {
                    objectTower.GetComponent<Tower>().NonBuildable();
                }
            }
            else
            {
                if (player.GetComponent<PlayerStats>().Gold < objectTower.GetComponent<Tower>().Price)
                {
                    Debug.Log("Insufficient funds");
                }
                objectTower.GetComponent<Tower>().NonBuildable();
            }
            Debug.DrawLine(ray.origin, hit.point, Color.red);
        }
    }
}