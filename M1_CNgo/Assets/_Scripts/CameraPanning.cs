﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanning : MonoBehaviour {

    public float PanSpeed;

    private Camera cam;

	// Use this for initialization
	void Start () {
        cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        if (mousePos.x < 0.1f || Input.GetKey(KeyCode.LeftArrow))
        {
            cam.transform.localPosition -= new Vector3(PanSpeed, 0, 0) * Time.deltaTime;
        }
        else if (mousePos.x > 0.9f || Input.GetKey(KeyCode.RightArrow))
        {
            cam.transform.localPosition += new Vector3(PanSpeed, 0, 0) * Time.deltaTime;
        }

        if (mousePos.y < 0.1f || Input.GetKey(KeyCode.DownArrow))
        {
            cam.transform.localPosition -= new Vector3(0, 0, PanSpeed) * Time.deltaTime;
        }
        else if (mousePos.y > 0.9f || Input.GetKey(KeyCode.UpArrow))
        {
            cam.transform.localPosition += new Vector3(0, 0, PanSpeed) * Time.deltaTime;
        }
    }
}
