﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tower : MonoBehaviour
{
    //Tower name
    public string Name;
    //Tower level
    public int Level;
    //Tower price
    public int Price;
    //Can Upgrade Tower
    public bool CanUpgrade;
    //Build Duration
    public float BuildDuration;
    //Material of the tower
    public Material towerMat;

    //Is the tower placed?
    public bool IsPlaced;
    //Is the tower selected?
    public bool IsSelected;

    //The possible upgrade of the tower
    public GameObject NextUpgrade;

    // Use this for initialization
    public virtual void Update()
    {
        CanUpgrade = NextUpgrade != null ? true : false;

        //If the tower is selected...
        if (IsSelected)
        {
            //Set the material of the tower to cyan
            towerMat.color = Color.cyan;
        }
        //Else set it back to normal
        else
        {
            //Set the material of the tower to white
            towerMat.color = Color.white;
        }

    }


    public virtual void Upgrade()
    {

        //There is a possible upgrade,
        if (NextUpgrade != null)
        {
            //Instantiate the new tower in the same place and rotation
            Instantiate(NextUpgrade, transform.position, Quaternion.identity);
            //And destroy the current tower
            Destroy(gameObject);
        }
    }

    public void Buildable()
    {
        //Set the material of the tower to green
        towerMat.color = Color.green;
    }

    public void NonBuildable()
    {
        //Set the material of the tower to red
        towerMat.color = Color.red;
    }

    public void Build()
    {
        //Set the tower as "placed"
        IsPlaced = true;
        //Set the material of the tower to white
        towerMat.color = Color.white;
        //Enable the collider of the tower
        GetComponent<Collider>().enabled = true;
    }
}
