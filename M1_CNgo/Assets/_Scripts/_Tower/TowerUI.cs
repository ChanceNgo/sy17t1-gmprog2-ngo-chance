﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TowerUI : MonoBehaviour
{

    public PlayerStats PlayerStats;
    public GameObject TowerSelected;
    public GameObject UpgradeButton;
    public Text TowerName;
    public Text TowerLevel;
    public Text TowerPriceUpgrade;
    public LayerMask Mask;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        DetectTower();

        if (TowerSelected)
        {
            TowerName.text = "Name: " + TowerSelected.GetComponent<Tower>().Name;
            TowerLevel.text = "Level: " + TowerSelected.GetComponent<Tower>().Level;
            TowerPriceUpgrade.text = "Upgrade Price: " + TowerSelected.GetComponent<Tower>().Price;
            if (TowerSelected.GetComponent<Tower>().CanUpgrade)
                UpgradeButton.SetActive(true);
        }
        else
        {
            TowerName.text = "Name: ";
            TowerLevel.text = "Level: ";
            TowerPriceUpgrade.text = "Upgrade Price: ";
            UpgradeButton.SetActive(false);
        }
    }

    void DetectTower()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, Mask, QueryTriggerInteraction.Ignore))//, LayerMask.NameToLayer("Towers")))//, QueryTriggerInteraction.Collide))
            {
                if (hit.collider.tag == "Tower")
                    TowerSelected = hit.collider.gameObject;
                else
                    TowerSelected = null;
            }
        }
    }

    public void UpgradeTower()
    {
        if (TowerSelected)
        {
            if (PlayerStats.Gold >= TowerSelected.GetComponent<Tower>().Price)
            {
                TowerSelected.GetComponent<Tower>().Upgrade();
                PlayerStats.DeductGold(TowerSelected.GetComponent<Tower>().Price);
            }
        }
    }
}
