﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public GameObject Player;

	// Use this for initialization
	void Start () {
		if(Player == null)
        {
            GameObject.FindGameObjectWithTag("Player");
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (Player)
        {
            transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, transform.position.z);

            float playerSize = Player.GetComponent<Size>().CurrentSize;
            GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, 5 + ((playerSize * playerSize) / 100), Time.deltaTime);
        }
    }
}
