﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float Speed;
    public Size PlayerSize;

	// Use this for initialization
	void Start () {
        PlayerSize = GetComponent<Size>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        float speedPerFrame = 2.2f * (Speed/2 * Mathf.Pow((PlayerSize.CurrentSize * PlayerSize.CurrentSize)/100, -0.5f));
        
        Vector3 moveVector = Vector3.Lerp(transform.position, mousePos, Time.deltaTime * speedPerFrame/35);

        moveVector.x = Mathf.Clamp(moveVector.x, -9, 9);
        moveVector.y = Mathf.Clamp(moveVector.y, -9, 9);
        moveVector.z = 0;
        transform.position = moveVector;
    }
}
