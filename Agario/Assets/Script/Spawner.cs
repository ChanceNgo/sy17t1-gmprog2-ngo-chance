﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject Prefab;

    public int MaxObjectsSpawned;
    public float SpawnRate;
    public Vector2 MapBoundary;
    public int InitialSpawned;

    private List<GameObject> prefabsSpawned;
    private float currentTime;

	// Use this for initialization
	void Start () {
        prefabsSpawned = new List<GameObject>();

        for(int i = 0; i < InitialSpawned; i++)
        {
            Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
            GameObject prefabs = (GameObject)Instantiate(Prefab, randPos, Quaternion.identity);
            prefabsSpawned.Add(prefabs);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (prefabsSpawned.Count < MaxObjectsSpawned)
        {
            if(currentTime > 0)
            {
                currentTime -= Time.deltaTime;
            }
            else if(currentTime <= 0)
            {
                Vector3 randPos = new Vector3(Random.Range(MapBoundary.x, -MapBoundary.x), Random.Range(MapBoundary.y, -MapBoundary.y), 0.0f);
                GameObject prefabs = (GameObject)Instantiate(Prefab, randPos, Quaternion.identity);
                prefabsSpawned.Add(prefabs);
                currentTime = SpawnRate;
            }
        }
	}
}
