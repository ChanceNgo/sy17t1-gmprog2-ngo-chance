﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

enum EnemyState
{
	Chase,
	Run,
	Patrol
}


public class EnemyAI : MonoBehaviour {

	[SerializeField]EnemyState eState = EnemyState.Patrol;
    public Transform nearByEnemy;
    public Vector3 Patrolpos;
    Size size;
    public void Start()
    {
        size = GetComponent<Size>();
        Patrolpos = new Vector3(Random.Range(-9, 9), Random.Range(-9, 9), 0);
        eState = EnemyState.Patrol;
    }

    void Detect()
    {
        List<Collider> detected = Physics.OverlapSphere(transform.position, size.CurrentSize * 4).ToList();

        for (int i = 0; i < detected.Count; i++)
        {

            if (detected[i].gameObject != gameObject)
            {
                detected.Remove(detected[i]);
            }

        }
        for (int i = 0; i < detected.Count; i++)
        {
            if (detected[i].gameObject.tag == "Player" )
            {

                nearByEnemy = detected[i].transform;
                eState = EnemyState.Chase;
                break;
            }
            if (detected[i].gameObject.tag == "Food")
            {
                Debug.Log("Detected food");
                nearByEnemy = detected[i].transform;
                
                eState = EnemyState.Chase;
                break;

            }

        }
    }

    void Update () 
	{
        Detect();
        switch (eState) 
		{
		case EnemyState.Patrol:
			UpdatePatrol ();
			break;
		case EnemyState.Chase:
			UpdateChase ();
			break;
		case EnemyState.Run:
			UpdateRun ();
			break;
		}
        if (!nearByEnemy)
            nearByEnemy = null;
    }


	void UpdateChase()
	{
		//code chase
		//decision - Run
		//decision - patrol

        if (nearByEnemy != null)
        {
                transform.position = Vector3.MoveTowards(transform.position, nearByEnemy.transform.position, Time.deltaTime);
        }
        if(Vector3.Distance(transform.position,nearByEnemy.transform.position)<1)
        {
            Patrolpos = new Vector3(Random.Range(-9, 9), Random.Range(-9, 9), 0);
            nearByEnemy = null;
            eState = EnemyState.Patrol;
        }

        Debug.Log ("Im Chasing");
	}

	void UpdateRun()
	{
        //code Run
        //decision - Chase
        //decision - patrol

        if (nearByEnemy != null)
        {
            Vector3 direction = (nearByEnemy.transform.position + transform.position);
            float distance = direction.magnitude;
            if (distance < 5)
            {
                transform.position = Vector3.MoveTowards(transform.position, direction, Time.deltaTime);
            }
            else
            {
                Patrolpos = new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0);
                eState = EnemyState.Patrol;
            }

        }
        Debug.Log ("Im Running");
	}

	void UpdatePatrol()
	{
        //code Patrol
        //decision - Chase
        //decision - Run

       
        transform.position = Vector3.MoveTowards(transform.position, Patrolpos, Time.deltaTime);
        if(Vector3.Distance(transform.position, Patrolpos) <2)
        {
            Patrolpos = new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), 0);
        }
        Debug.Log ("Im Patrolling");
	}
}
